# SVIPT

### Scalable Vector Icon for Platform Tool

This tiny command line tool takes svg file as an input to prepare app icon files for different platforms. For now these platforms are: IOS, Android, Windows (good old win32 ico format). Microsoft UWP and Apple Mac OSX platform are to come.

#### How to use

```
svipt -in test.svg -android_dir android -android_name icon -ios_dir ios -ico_file icon.ico
```
This command will generate set of icon files in proper formats and sizes for three different platforms.


#### How to build
Install haxe from http://haxe.org/download

Install hxcpp for native version (also native compiler needed: msvs|gcc|xcode):
```
haxelib install hxcpp
```
OpenFL and its dependencies:
```
haxelib install openfl
haxelib run openfl setup
```
Other dependencies:
```
haxelib install svg
haxelib install arguable
```
Build command:
```
haxe build.hxml
haxe build_neko.hxml
```

Hope this tool will be useful for you.
