@echo off

call build.cmd
if errorlevel 1 echo build failed & exit /b 1

set /p version=<VERSION

set packagedir=svipt_%version%
set package=%packagedir%.zip
del /q %package%
rmdir /s /q %packagedir%
mkdir %packagedir%

copy /y "bin\Main.exe" "%packagedir%\svipt.exe"
copy /y "LICENSE" "%packagedir%"
copy /y "VERSION" "%packagedir%"
copy /y "readme.md" "%packagedir%"

set haxelib=%HAXEPATH%\lib

set hxcpp=%haxelib%\hxcpp\3,2,205\bin\Windows\
copy /y "%hxcpp%\regexp.dll" "%packagedir%"
copy /y "%hxcpp%\std.dll" "%packagedir%"
copy /y "%hxcpp%\zlib.dll" "%packagedir%"

set lime=%haxelib%\lime\2,9,0\ndll\Windows\
copy /y "%lime%\lime.ndll" "%packagedir%"

"%ProgramFiles%\7-zip\7z.exe" a %package% %packagedir%
rmdir /s /q %packagedir%
