@echo off

rmdir /s /q out

set args=-in test.svg -base_dir out -android_dir android -android_name icon -ios_dir ios -ios_contents Contents.json -ico_file icon.ico -png_file icon.png -jpg_file icon.jpg -verbose

rem copy /y bin\Main.exe svipt.exe
rem set svipt=svipt.exe
set svipt=bin\Main.exe

if not "%~1"=="" (
    if %1 == debug (
        rem copy /y bin\Main-debug.exe sviptd.exe
        rem set svipt=sviptd.exe
        set svipt=bin\Main-debug.exe
    )
)

rem set svipt="neko svipt.n"

call "%svipt%" %args%

rem "%ProgramFiles(x86)%\xnview\xnview" out\android\drawable-xxxhdpi\icon.png
rem "%ProgramFiles(x86)%\xnview\xnview" out\ios\iphone_60_3x.png
rem "%ProgramFiles(x86)%\xnview\xnview" out\icon.ico
rem "%ProgramFiles(x86)%\xnview\xnview" out\icon.png
rem "%ProgramFiles(x86)%\xnview\xnview" out\icon.jpg
