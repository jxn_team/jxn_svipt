package;


import arguable.ArgParser;
import format.SVG;
import haxe.io.Bytes;
import haxe.io.Path;
import haxe.Json;
import haxe.PosInfos;
import lime.graphics.format.BMP;
import lime.graphics.Image;
import openfl.display.BitmapData;
import openfl.display.Shape;
import sys.FileSystem;
import sys.io.File;


class Main
{
    static var args:ArgValues;
    static var verbose = false;
    static var svg:SVG;
    static var images:Map<Int,Image> = new Map();
    static var bytes:Map<ByteType,Map<Int,Bytes>> = [ BT_BMP => new Map(), BT_JPG => new Map(), BT_PNG => new Map() ];


    static function run()
    {
        ArgParser.delimiter = '-';
        args = ArgParser.parse(Sys.args());
        verbose = args.has('verbose');
        if(args.has('help') || args.has('version'))
            throw '';

        if( !args.has('in') )
            throw 'required argument "in"';

        var inPath = Path.normalize(args.get('in').value);

        if( !FileSystem.exists(inPath) )
            throw 'path for "in" argument does not exist: "' + inPath + '"';

        if( FileSystem.isDirectory(inPath) )
            throw 'path for "in" argument is a directory: "' + inPath + '"';

        var baseDir:String = null;
        if( args.has('base_dir') )
        {
            baseDir = Path.normalize(args.get('base_dir').value);
            if( FileSystem.exists(baseDir) )
            {
                if( !FileSystem.isDirectory(baseDir) )
                    throw 'path for "base_dir" argument exists and is not directory: "' + baseDir + '"';
            }
            else
            {
                try {
                    FileSystem.createDirectory(baseDir);
                }
                catch(error:Dynamic) {
                    throw 'could not create directory for "our_dir" argument: "' + baseDir + '"';
                }
            }
        }

        var baseName:String = Path.withoutDirectory(Path.withoutExtension(inPath));

        var androidDir:String = null;
        var iosDir:String = null;
        var icoFile:String = null;
        var pngFile:String = null;
        var jpgFile:String = null;
        var doAndroid = false;
        var doIos = false;
        var doIco = false;
        var doPng = false;
        var doJpg = false;

        if( args.has('android_dir') )
        {
            androidDir = Path.normalize(args.get('android_dir').value);
            doAndroid = true;
        }
        if( args.has('ios_dir') )
        {
            iosDir = Path.normalize(args.get('ios_dir').value);
            doIos = true;
        }
        if( args.has('ico_file') )
        {
            icoFile = Path.normalize(args.get('ico_file').value);
            doIco = true;
        }
        if( args.has('png_file') )
        {
            pngFile = Path.normalize(args.get('png_file').value);
            doPng = true;
        }
        if( args.has('jpg_file') )
        {
            jpgFile = Path.normalize(args.get('jpg_file').value);
            doJpg = true;
        }

        if( !doAndroid && !doIos && !doIco && !doPng && !doJpg )
            throw 'required one or more platform arguments: "android_dir", "ios_dir", "ico_file", "png_file", "jpg_file"';

        log('reading svg: "' + inPath + '"');
        var inContent:String = null;
        try {
            inContent = File.getContent(inPath);
        }
        catch(error:Dynamic) {
            throw 'could not read file for "in" argument: "' + inPath + '"';
        }
        try {
            svg = new SVG(inContent);
        }
        catch(error:Dynamic) {
            throw 'error reading "in" argument as svg: "' + inPath + '": "' + error + '"';
        }
        log('svg read ok');

        if( doAndroid )
        {
            if( baseDir != null )
            {
                if( androidDir == null )
                    androidDir = Path.join([baseDir, 'android']);
                else
                    androidDir = Path.join([baseDir, androidDir]);
            }
            var androidName:String = null;
            if( args.has('android_name') )
                androidName = args.get('android_name').value;
            else
                androidName = baseName;
            log('exporting for android: "' + androidDir + '"');
            exportAndroid(androidDir, androidName);
            log('export for android ok');
        }

        if( doIos )
        {
            if( baseDir != null )
            {
                if( iosDir == null )
                    iosDir = Path.join([baseDir, 'ios']);
                else
                    iosDir = Path.join([baseDir, iosDir]);
            }
            var contentsPath:String = null;
            if( args.has('ios_contents') )
                contentsPath = Path.normalize(args.get('ios_contents').value);
            log('exporting for ios: "' + iosDir + '"');
            exportIos(iosDir, contentsPath);
            log('export for ios ok');
        }

        if( doIco )
        {
            if( baseDir != null )
            {
                if( icoFile == null )
                    icoFile = Path.withExtension(Path.join([baseDir, baseName]), 'ico');
                else
                    icoFile = Path.join([baseDir, icoFile]);
            }
            log('exporting ico: "' + icoFile + '"');
            exportIco(icoFile);
            log('export ico ok');
        }

        if( doPng )
        {
            if( baseDir != null )
            {
                if( pngFile == null )
                    pngFile = Path.withExtension(Path.join([baseDir, baseName]), 'png');
                else
                    pngFile = Path.join([baseDir, pngFile]);
            }
            var pngSize:Int = 512;
            if( args.has('png_size') )
                pngSize = Std.parseInt(args.get('png_size').value);
            var pngBackground:Int = 0xFFFFFF;
            if( args.has('png_background') )
                pngBackground = Std.parseInt(args.get('png_background').value);
            log('exporting png: "' + pngFile + '"');
            savePng(pngSize, pngFile, !args.has('png_opaque'), pngBackground);
            log('export png ok');
        }

        if( doJpg )
        {
            if( baseDir != null )
            {
                if( jpgFile == null )
                    jpgFile = Path.withExtension(Path.join([baseDir, baseName]), 'png');
                else
                    jpgFile = Path.join([baseDir, jpgFile]);
            }
            var jpgSize:Int = 512;
            if( args.has('jpg_size') )
                jpgSize = Std.parseInt(args.get('jpg_size').value);
            var jpgBackground:Int = 0xFFFFFF;
            if( args.has('jpg_background') )
                jpgBackground= Std.parseInt(args.get('jpg_background').value);
            var jpgQuality:Int = 100;
            if( args.has('jpg_quality') )
                jpgQuality = Std.parseInt(args.get('jpg_quality').value);
            log('exporting jpg: "' + jpgFile + '"');
            saveJpg(jpgSize, jpgFile, jpgBackground, jpgQuality);
            log('export jpg ok');
        }
    }


    static function exportAndroid(dir:String, name:String)
    {
        var iconTypes = [ 'ldpi', 'mdpi', 'hdpi', 'xhdpi', 'xxhdpi', 'xxxhdpi' ];
        var iconSizes = [ 36, 48, 72, 96, 144, 192 ];
        for( i in 0...iconTypes.length )
            savePng(iconSizes[i], Path.join([dir, 'drawable-' + iconTypes[i], name + '.png']));
    }


    static function exportIos(dir:String, contentsPath:String)
    {
        var iconInfos:Array<IconInfo> = null;
        if( contentsPath == null )
        {
            log('no ios contents file was specified\nusing default contents settings');
        }
        else
        {
            var contensData:String = null;
            try {
                contensData = File.getContent(contentsPath);
            }
            catch(error:Dynamic) {
                log('can\'t read ios contents file "' + contentsPath + '"\nusing default contents settings');
            }
            if( contensData != null )
            {
                if( contensData.length == 0 )
                {
                    log('ios contents file "' + contentsPath + '" is empty\nusing default contents settings');
                }
                else
                {
                    try {
                        iconInfos = parseIconInfo(contensData);
                        log('ios contents file: "' + contentsPath + '"');
                    }
                    catch(error:Dynamic) {
                        log('can\'t parse contents file "' + contentsPath + '": "' + error + '"\nusing default contents settings');
                    }
                }
            }
        }
        if( iconInfos == null )
            iconInfos = getDefaultIosIconInfos();
        for( iconInfo in iconInfos )
            savePng(iconInfo.size, Path.join([dir,iconInfo.name]));
    }


    static function parseIconInfo(source:String):Array<IconInfo>
    {
        var contens:ContentsObj = Json.parse(source);
        var result = new Array();
        if( contens.images == null || contens.images.length == 0 )
            throw 'no images found';
        for( image in contens.images )
        {
            if( image.size == null || image.scale == null || image.filename == null && image.idiom == null )
                continue;
            var size = Std.parseInt(image.size.substring(0, image.size.indexOf("x")));
            var name:String = image.filename;
            if( name == null || name.length == 0 )
                name = image.idiom + '_' + size + '_' + image.scale + '.png';
            var scale = Std.parseInt(image.scale.substring(0, image.scale.indexOf("x")));
            size = size * scale;
            result.push({ name : name, size : size });
        }
        if( result.length == 0 )
            throw 'no images were processed';
        return result;
    }


    static function getDefaultIosIconInfos():Array<IconInfo>
    {
        return [
            { name : "iphone_29_2x.png", size : 58 },
            { name : "iphone_29_3x.png", size : 87 },
            { name : "iphone_40_2x.png", size : 80 },
            { name : "iphone_40_3x.png", size : 120 },
            { name : "iphone_60_2x.png", size : 120 },
            { name : "iphone_60_3x.png", size : 180 },
            { name : "ipad_29_1x.png", size : 29 },
            { name : "ipad_29_2x.png", size : 58 },
            { name : "ipad_40_1x.png", size : 40 },
            { name : "ipad_40_2x.png", size : 80 },
            { name : "ipad_76_1x.png", size : 76 },
            { name : "ipad_76_2x.png", size : 152 },
        ];
    }


    static function exportIco(path:String)
    {
        var sizes = [ 16, 24, 32, 40, 48, 64, 96, 128, 256, 512, 768 ];
        var datas = new Array<Bytes>();
        for( size in sizes )
            datas.push( if( size < 256 ) getBytes(BT_BMP, size); else getBytes(BT_PNG, size) );
        var length = 6 + (16 * sizes.length);
        for( data in datas )
            length += data.length;
        var icon = Bytes.alloc(length);
        var position = 0;
        icon.setUInt16(position, 0);                position += 2;
        icon.setUInt16(position, 1);                position += 2;
        icon.setUInt16(position, sizes.length);    position += 2;
        var dataOffset = 6 + (16 * sizes.length);
        for( i in 0...sizes.length )
        {
            var size = sizes[i];
            icon.set(position++, size > 255 ? 0 : size);
            icon.set(position++, size > 255 ? 0 : size);
            icon.set(position++, 0);
            icon.set(position++, 0);
            icon.setUInt16(position, 1);                    position += 2;
            icon.setUInt16(position, 32);                   position += 2;
            icon.setInt32(position, datas[i].length);       position += 4;
            icon.setInt32(position, dataOffset);            position += 4;
            dataOffset += datas[i].length;
        }
        for( data in datas )
        {
            icon.blit(position, data, 0, data.length);
            position += data.length;
        }
        saveBytes(icon, path);
    }


    static function savePng(size:Int, path:String, ?transparent:Bool, ?background:Int)
    {
        saveBytes(getBytes(BT_PNG, size, transparent, background), path);
    }


    static function saveJpg(size:Int, path:String, background:Int, quality:Int)
    {
        saveBytes(getBytes(BT_JPG, size, false, background, quality), path);
    }


    static function saveBytes(bytes:Bytes, path:String)
    {
        log('saving file: "' + path + '"');
        try {
            var dir = Path.directory(path);
            if( dir != null && dir.length > 0 )
                FileSystem.createDirectory(dir);
        }
        catch(error:Dynamic) {
            throw 'could not create directory: "' + path + '"';
        }
        try {
            File.saveBytes(path, bytes);
        }
        catch(error:Dynamic) {
            throw 'could not save file: "' + path + '"';
        }
    }


    static function getBytes(type:ByteType, size:Int, ?transparent:Bool, ?background:Int, ?quality:Int)
    {
        if( bytes.get(type).exists(size) )
            return bytes.get(type).get(size);
        var image = getImage(size, transparent, background);
        var result = switch( type )
        {
        case BT_BMP: BMP.encode(image, BMPType.ICO);
        case BT_JPG: image.encode('jpg', quality);
        case BT_PNG: image.encode('png');
        };
        if( result == null )
        {
            throw 'error encoding ' + type + ' for size: ' + size + '"';
            return null;
        }
        else
        {
            bytes.get(type).set(size, result);
            return result;
        }
    }


    static function getImage(size:Int, ?transparent:Bool, ?background:Int)
    {
        var useCache = transparent != null || background != null;
        if( useCache && images.exists(size) )
            return images.get(size);

        var shape = new Shape();
        svg.render(shape.graphics, 0, 0, size, size);
        var bitmapData = new BitmapData(size, size, transparent == null ? true : transparent, background == null ? 0xFFFFFF : background);
        bitmapData.draw(shape);
        var image = Image.fromBitmapData(bitmapData);
        if( useCache )
            images.set(size, image);
        return image;
    }


    static function log(msg:Dynamic, ?infos:PosInfos)
    {
        if( !verbose )
            return;
        Sys.println(msg);
        //haxe.Log.trace(msg, infos);
    }


    static function main()
    {
        try {
            run();
        }
        catch(error:Dynamic) {
            Sys.println(error);
            showUsage();
            Sys.exit(1);
        }
    }


    static inline public var VERSION = Macro.getVersion();


    static function showUsage()
    {
        Sys.println('svipt: Scalable Vector Icon for Platform Tool');
        Sys.println('version: ' + VERSION);
        Sys.println('arguments:');
        Sys.println('  -in <path>            :: path to input svg file');
        Sys.println('  -base_dir <dir>       :: dir prefix for all output');
        Sys.println('  -android_dir <dir>    :: output dir for android');
        Sys.println('  -android_name <name>  :: name for output icon files for android');
        Sys.println('  -ios_dir <dir>        :: output dir for ios');
        Sys.println('  -ios_contents <name>  :: path to input ios Contents.json file');
        Sys.println('  -ico_file <path>      :: path to output ico file');
        Sys.println('  -png_file <path>      :: path to output png file');
        Sys.println('  -png_size <int>       :: size of svg render for png (default: 512)');
        Sys.println('  -png_opaque           :: if present then png render will be opaque (default: transparent)');
        Sys.println('  -png_background <int> :: color for opaque svg render for png, maybe 0xZZZZZZ (default: 0xFFFFFF)');
        Sys.println('  -jpg_file <path>      :: path to output jpg file');
        Sys.println('  -jpg_size <int>       :: size of svg render for jpg (default: 512)');
        Sys.println('  -jpg_background <int> :: color for svg render for png, maybe 0xZZZZZZ (default: 0xFFFFFF)');
        Sys.println('  -jpg_quality <int>    :: quality for jpg encoding (default: 100)');
        Sys.println('  -verbose              :: if present then additional log messages will be showed');
        Sys.println('  -help || -version     :: show this text');
    }
}


private typedef IconInfo = {
    name:String,
    size:Int,
}


private enum ByteType {
    BT_BMP;
    BT_JPG;
    BT_PNG;
}


private typedef ContentsObj = {
    images:Array<ContentsImage>,
}


private typedef ContentsImage = {
    idiom:String,
    size:String,
    scale:String,
    filename:String,
}
