package;

import haxe.io.Path;
import haxe.macro.Context;
import haxe.macro.Expr;
import sys.FileSystem;
import sys.io.File;


class Macro
{
    static private inline var VERSION_PATH = "./VERSION";

    macro static public function getVersion()
    {
        if( !FileSystem.exists(VERSION_PATH) || FileSystem.isDirectory(VERSION_PATH) )
            throw new Error('version file not found', Context.currentPos());

        var version = File.getContent("./VERSION");
        if( version == null || version.length == 0 )
            throw new Error('cannot read version file or it\'s empty', Context.currentPos());

        return macro $v{version};
    }
}
